const axios = require('axios').default;

const QuestionIds = []
const success = document.getElementById("success")
const submitbtn = document.getElementById("btnid")
axios.get('http://localhost:3000/questions')
  .then(function (response) {


    const opt = response.data[0].options;
    const len = response.data.length;
    const fullDiv = document.getElementById("questions");
    const mainDiv = document.createElement("div");
    const queshd = document.createElement("h1");
    queshd.innerHTML = "QUESTIONS";
    mainDiv.append(queshd);
    for (let j = 0; j < len; j++) {

      QuestionIds.push(response.data[j]._id)

      const quesnum = document.createElement("h4");
      const qDiv = document.createElement("div")
      qDiv.classList.add("QDiv")
      quesnum.innerHTML = response.data[j].question;
      qDiv.append(quesnum);
      const opt = response.data[j].options;
      const option = document.createElement("div");
      option.classList.add("options")

      for (let i = 0; i <= 3; i++) {

        const inputdiv = document.createElement("div")
        inputdiv.innerHTML = `<input type = "radio" id="${response.data[j]._id + i}" value = ${opt[i]} name = "${response.data[j]._id}">
        <label for="${response.data[j]._id + i}">  ${opt[i]}  </label>`
        option.append(inputdiv);


      }
      const clrButton = document.createElement('input')
      clrButton.type = "button"
      clrButton.id = response.data[j]._id
      clrButton.classList.add("clrbtn")
      clrButton.value = 'Clear Choice'

      clrButton.addEventListener('click', (e) => {
        e.preventDefault()
        document.querySelector(`input[name="${clrButton.id}"]:checked`).checked = false;
      })

      qDiv.append(option)
      qDiv.append(clrButton)
      mainDiv.append(qDiv)
      fullDiv.append(mainDiv)
      submitbtn.classList.remove("hidden")
    }
  })
  .catch(function (error) {
    console.log(error);
  })
const flag = false;
const form = document.getElementById("form")
form.addEventListener('submit', (e) => {
  e.preventDefault()
  const attempt = QuestionIds.map(qId => {
    return {
      question: qId,
      selectedOPT: form[qId].value === '' ? null : form[qId].value
    }
  })
  const payload = {
    questions: attempt
  }

  const check = attempt.filter(q => q.selectedOPT !== null)
  // console.log(check)

  if ((check.length) == 0) {
    alert("Attempt atleast one question")
  }
  else {

    success.classList.remove("success")
    axios.post('http://localhost:3000/useroption', payload)
      .then(data => {
        console.log(data)
        console.log(attempt)
      })
      .catch(error => {
        console.log(error)
      })
  }
})






