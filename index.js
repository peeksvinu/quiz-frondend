const fullDiv = document.getElementById("homepg")
const eng = document.getElementById("eng")
const headfile = [
  {
    "name": "IBPS RRB PO Mains Free Mock Test",
    "pic": "https://cdn.siasat.com/wp-content/uploads/2020/01/IBPS.jpg"
  },
  {
    "name": "SSC CGL Tier I Free Mock Test",
    "pic": "https://www.ssccoachingchandigarh.in/assets/upload/course/SSC_CGL_Logo1.png"
  },
  {
    "name": "SSC CHSL Tier I Free Mock Test",
    "pic": "https://oldquestionpapers.org/wp-content/uploads/2020/03/SSC-CHSL.jpg"
  },
  {
    "name": "SSC CPO (Tier 1) Free Mock Test",
    "pic": "https://oldquestionpapers.org/wp-content/uploads/2020/03/SSC-CPO.jpg"
  },
  {
    "name": "SSC Constable (GD) Free Mock Test",
    "pic": "https://th.bing.com/th/id/OIP.rqoixbjGN0uwOaTUqFUBSAAAAA?pid=ImgDet&rs=1"
  },
  {
    "name": "SSC MTS Free Mock Test",
    "pic": "https://www.entranceexams.io/wp-content/uploads/2017/04/ssc-mts-image.jpg"
  }

]
for (let i = 0; i < headfile.length; i++) {

  const posterDiv = document.createElement("div")
  posterDiv.classList.add("poster-cls")
  const posterhead = document.createElement("b")
  posterhead.innerHTML = headfile[0].name
  const posterimg = document.createElement("img")
  posterimg.classList.add("poster-img")
  posterimg.src = headfile[i].pic

  const enterbtn = document.createElement('input')
  enterbtn.type = "button"
  enterbtn.classList.add("enter-btn")
  enterbtn.id = headfile[i].id
  enterbtn.value = 'CLICK To Enter'


  enterbtn.addEventListener("click", function () {
    document.location.href = './homepg.html';
    console('something');
  });

  eng.addEventListener("click", function () {
    document.location.href = './englishsub.html';
  });

  posterDiv.append(posterhead)
  posterDiv.append(posterimg)
  posterDiv.append(enterbtn)
  fullDiv.append(posterDiv)
}
